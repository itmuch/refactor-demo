package com.example.refactordemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RefactorDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(RefactorDemoApplication.class, args);
    }

}
